use std::collections::HashMap;
use std::convert::TryFrom;
use std::error::Error;
use std::fmt;
use std::fs::File;
use std::io::Read;
use std::path::Path;

/// Catch-all return type.
type Result<T> = std::result::Result<T, Box<dyn Error>>;

/// Write the contents of a file to a string.
fn file_to_string<P>(filename: P) -> Result<String>
where
    P: AsRef<Path>,
{
    let mut file = File::open(filename)?;
    let mut file_contents = String::new();
    file.read_to_string(&mut file_contents)?;
    Ok(file_contents)
}

/// Load a vector of `TestResult` objects from a file.
fn load_results<P>(filename: P) -> Result<Vec<TestResult>>
where
    P: AsRef<Path>,
{
    let file = file_to_string(filename)?;
    file.lines()
        .map(|x| TestResult::try_from(x))
        .collect::<Result<Vec<TestResult>>>()
}

/// A student's result for a specific test.
#[derive(Debug)]
enum TestResult {
    Attended(String, i32),
    Missed(String),
}

/// Try generating a `TestResult` from a string.
impl TryFrom<&str> for TestResult {
    type Error = Box<dyn Error>;
    fn try_from(in_string: &str) -> Result<Self> {
        let delimiter = ':';
        if in_string.contains(delimiter) {
            let splitted_string: Vec<&str> = in_string.split(delimiter).collect();
            let name = String::from(splitted_string[0]);
            let score: i32 = splitted_string[1].parse()?;
            return Ok(TestResult::Attended(name, score));
        }
        Ok(TestResult::Missed(String::from(in_string)))
    }
}

/// A student's overall performance.
#[derive(Default, Debug)]
struct StudentReport {
    total_score: i32,
    tests_taken: i32,
    tests_missed: i32,
}

impl StudentReport {
    /// Add the score from a taken test.
    pub fn add_score(&mut self, score: i32) {
        self.total_score += score;
        self.tests_taken += 1;
    }
    /// Make note of a missed test.
    pub fn add_missed(&mut self) {
        self.tests_missed += 1;
    }
}

/// Display a `StudentReport` in human-friendly form.
impl fmt::Display for StudentReport {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let test_word = if self.tests_taken == 1 { "test" } else { "tests" };
        write!(
            f,
            "{} {} with a total score of {}. They missed {}.",
            self.tests_taken, test_word, self.total_score, self.tests_missed
        )
    }
}

/// Create a `HashMap` with the keys being student names and the values being their report.
/// Takes as input a vector of `TestResult` objects.
fn compile_reports(test_results: Vec<TestResult>) -> HashMap<String, StudentReport> {
    let mut reports: HashMap<String, StudentReport> = HashMap::new();
    for result in test_results {
        match result {
            TestResult::Attended(name, score) => reports
                .entry(name)
                .or_insert(Default::default())
                .add_score(score),
            TestResult::Missed(name) => reports
                .entry(name)
                .or_insert(Default::default())
                .add_missed(),
        };
    }
    reports
}

/// Iterate through student reports and print the data in English
fn print_reports(student_reports: HashMap<String, StudentReport>) {
    for (student, report) in student_reports {
        println!("{} took {}", student, report)
    }
}

fn main() -> Result<()> {
    let filename = std::env::args().nth(1).ok_or("Please give file name as command-line argument")?;
    let test_results = load_results(filename)?;

    // println!("{:?}", test_results);

    let student_reports = compile_reports(test_results);

    // println!("{:?}", student_reports);

    print_reports(student_reports);

    Ok(())
}

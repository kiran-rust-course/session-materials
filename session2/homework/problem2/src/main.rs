use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::fmt;

/// Catch-all return type.
type Result<T> = std::result::Result<T, Box<dyn Error>>;

/// Raw brainfuck instructions
#[derive(Debug)]
enum RawInstruction {
    /// \>
    RightAngular,
    /// <
    LeftAngular,
    /// +
    Plus,
    /// -
    Dash,
    /// -
    Period,
    /// ,
    Comma,
    /// [
    LeftSquare,
    /// ]
    RightSquare,
}

/// Convert the corresponding character into a [`RawInstruction`]
impl RawInstruction {
    fn from_char(c: char) -> Option<RawInstruction> {
        match c {
            '>' => Some(RawInstruction::RightAngular),
            '<' => Some(RawInstruction::LeftAngular),
            '+' => Some(RawInstruction::Plus),
            '-' => Some(RawInstruction::Dash),
            '.' => Some(RawInstruction::Period),
            ',' => Some(RawInstruction::Comma),
            '[' => Some(RawInstruction::LeftSquare),
            ']' => Some(RawInstruction::RightSquare),
            _ => None,
        }
    }
}

/// Get a description of the effect of the [`RawInstruction`]
fn raw_to_string(raw_instruction: &RawInstruction) -> String {
    match raw_instruction {
        RawInstruction::RightAngular => String::from("Increment the data pointer (to point to the next cell to the right)."),
        RawInstruction::LeftAngular => String::from("Decrement the data pointer (to point to the next cell to the left)."),
        RawInstruction::Plus => String::from("Increment (increase by one) the byte at the data pointer."),
        RawInstruction::Dash => String::from("Decrement (decrease by one) the byte at the data pointer."),
        RawInstruction::Period => String::from("Output the byte at the data pointer."),
        RawInstruction::Comma => String::from("Accept one byte of input, storing its value in the byte at the data pointer."),
        RawInstruction::LeftSquare => String::from("If the byte at the data pointer is zero, then instead of moving the instruction pointer forward to the next command, jump it forward to the command after the matching ] command."),
        RawInstruction::RightSquare => String::from("If the byte at the data pointer is nonzero, then instead of moving the instruction pointer forward to the next command, jump it back to the command after the matching [ command. "),
    }
}

/// A brainfuck instruction
#[derive(Debug)]
struct Instruction {
    /// The raw instruction
    raw_instruction: RawInstruction,
    /// The line where the instruction was found
    line: usize,
    /// The column where the instruction was found
    column: usize,
    /// The file where the instruction was found
    filename: String,
}

impl Instruction {
    /// Create an [`Instruction`] object
    fn new(raw_instruction: RawInstruction, line: usize, column: usize, filename: String) -> Self {
        Self {
            raw_instruction,
            line,
            column,
            filename,
        }
    }
}

/// Print `Instruction` in human-friendly form
impl fmt::Display for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let character = raw_to_string(&self.raw_instruction);
        write!(
            f,
            "[{}:{}:{}]{}", self.filename, self.line, self.column, character
        )
    }
}

/// Write the contents of a file to a string.
fn file_to_string<P>(filename: P) -> Result<String>
where
    P: AsRef<Path>,
{
    let mut file = File::open(filename)?;
    let mut file_contents = String::new();
    file.read_to_string(&mut file_contents)?;
    Ok(file_contents)
}

/// Load a vector of [`Instruction`] objects from a file.
fn load_instructions<P>(filename: P) -> Result<Vec<Instruction>>
where
    P: AsRef<Path>,
    P: std::fmt::Display
{
    let file = file_to_string(&filename)?;
    let mut instructions: Vec<Instruction> = Vec::new();
    for (line_number, line) in file.lines().enumerate() {
        for (column_number, character) in line.chars().enumerate() {
            if let Some(raw_instruction) = RawInstruction::from_char(character) {
                instructions.push(Instruction::new(
                    raw_instruction,
                    line_number,
                    column_number,
                    filename.to_string(),
                ))
            }
        }
    };

    Ok(instructions)

}

/// Read in file name from command line and print the brainfuck instructions contained therein
fn main() -> Result<()> {
    let filename = std::env::args().nth(1).ok_or("Please give file name as command-line argument")?;
    let instructions = load_instructions(filename)?;
    // println!("{:?}", instructions);
    for instruction in instructions {
        println!("{}", instruction)
    }
    Ok(())
}

# Homework notes

* Panic is for programmer errors (going to far in a vector)
For all other errors return Result.

* Unit-style enums are what we get in see. It each variant is assigned int in order.

* Use '///' for doc strings

* Option enums are 1-field tuples

* In structs you can put values. If you wanna add functions then use `impl`

* &self is borrowing an instance of the Self type

* impl are behaviour and traits are a way to require a behaviour. Rust has no inheritance!

* Pass by owernship: function will consume variable, it will no longer exist after.
* Pass by borrow: variable will still exist after.

* `fn green(who: &impl bleh)` - compiler will create copy of green for every thing that implements `bleh`
* `fn green(who: &dyn bleh)` - compiler will create copy of green at run time (or something like that?)

* build vs run cost
[Learning materials](https://platypus.pepperfish.net/~dsilvers/rust-workshop-4/group-2/)

## Solutions

### Session 1

* [problem 1](session1/homework/problem1/)
* [problem 2](session1/homework/problem2/)

### Session 2

* [problem 1](session2/homework/problem1/)
* [problem 2](session2/homework/problem2/)
use std::env;

fn say_hello(name: &str) {
    println!("Hello, {}", name);
}

fn main() {
    for name in env::args().skip(1) {
        say_hello(&name);
    }
}

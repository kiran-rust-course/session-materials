fn verse(num: u32) {
    let bottles = if num == 1 { "bottle" } else { "bottles" };
    println!("{} {}", num, bottles);
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let verse_count: u32 = match std::env::args().nth(1)
    {
        Some(verse_count) => verse_count.parse()?,
        None => panic!("lolzor")
    };

    for num in (1..=verse_count).rev() {
        verse(num);
    }

    Ok(())
}
